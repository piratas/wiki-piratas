**Infraestrutura de Tecnologia da Informação**

Esta página visa organizar e identificar os recursos computacionais do Partido Pirata, com a dupla finalidade de publicizar a informação e para organização interna do [[GTI]].

[[!toc levels=6]]

---

# Servidores

Servidores são [sistemas de computação que fornecem serviços a uma rede de computadores](https://pt.wikipedia.org/wiki/Servidor).

## [entwickler](https://we.riseup.net/iikb/iti#entwickler-iikb-org).partidopirata.org

Servidor dedicado [KS-2A](http://www.kimsufi.com/pt/servidores.xml) providenciado até outubro de 2016 através [deste processo](https://www.loomio.org/d/F1eIYbtL/)

Basicamente tudo o que estiver redirecionando para **entwickler.partidopirata.org** e **piratas.iikb.org** está neste servidor.

### Financiamento

Este servidor foi financiado individualmente pelo [[felipe]] ao Partido até outubro de 2016.

### Endereços IPs públicos

#### IPv4

    5.135.182.223

#### IPv6

    2001:41d0:8:bddf::1

### Serviços

#### Nginx

##### Proxy

###### Redirecionamentos

[[!table class="table table-bordered table-striped" data="""
URL|Aponta para
<https://loomio.partidopirata.org>|<https://www.loomio.org/g/Hmpbu4Tr/>
<https://gti.partidopirata.org/github>|<http://piratas.github.io/gti>
<https://gti.partidopirata.org/loomio>|<https://www.loomio.org/g/JDmI4ZKv/>
<https://gti.partidopirata.org/notabug>|<https://notabug.org/piratas/organizacao/issues>
<https://gti.partidopirata.org/taiga>|<https://tree.taiga.io/project/tipirata/>
"""]]

#### Mediagoblin

##### Biblioteca Pirata

Disponível em:

* <https://biblioteca.partidopirata.org>
* <https://biblioteca.partidopirata.xyz>
* <https://biblioteca.pirata.xyz>

#### GNU Social

Disponível em:

* <https://social.pirata.xyz>

#### Python

##### Django

###### Hotsite coleta de assinaturas

Disponível em:

* <https://apoio.partidopirata.org>

##### Web2Py

*`TODO`: Atualizar serviços com web2py*

#### Hospedagem

##### Sites do Partido Pirata oficiais

* apoio.partidopirata.org
* biblioteca.partidopirata.org (mediagoblin)
* gti.partidopirata.org
* iuf.partidopirata.org
* mumble.partidopirata.org

##### Sites do Partido Pirata não oficiais

* partidopirata.xyz
* piratas.xyz
* social.pirata.xyz (gnu social)

##### Outros sites

###### lapirata

* lapirata.org
* lapirata.xyz

###### naofo.de

* nao.usem.xyz
* naofode.xyz
* nfde.xyz

#### Backup

Atualmente armazenando temporariamente os backups dos servidores suecos para análise e migração.


---

## bastidores.org

Dois servidores na europa (islandia.bastidores.org e suecia.bastidores.org) disponibilizados pelo [[mtoledo]].

### Financiamento

`TODO`: adicionar informações de financiamento

### Endereços IPs públicos

`TODO`: adicionar informações de rede

### Serviços

#### Hospedagem

##### Site do GTC

* <http://gtc.partidopirata.org>

##### Site da Assembleia Nacional 2016

II ANAPIRATA

* <http://anapirata.partidopirata.org>

##### Campanha Internet Livre

* <http://internetlivre.cc>

#### Backup

Backup do site do Partido - **partidopirata.org**

---

*`TODO`: adicionar servidores da suecia*

[[!meta title="Infraestrutura de Tecnologia da Informação"]]
[[!tag gti todo pendente]]
