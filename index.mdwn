Este espaço serve de apoio ao [PIRATAS - partido pirata no brasil][0].

A wiki está em processo de reconstrução. Quem tiver backups, material, vontade de ajudar, por favor encaminhe ao [GTI][1].

---

Este site está disponível em:

* <https://wiki.partidopirata.org>
* <https://partidopirata.xyz>
* <https://piratas.xyz>
* <https://pirata.xyz>

Este site utiliza [ikiwiki](https://ikiwiki.info), que compila a wiki e o banco de dados é na verdade um repositório GIT. Portanto, o código fonte deste site é atualizado automaticamente a cada *commit* e está disponível em:

* <https://notabug.org/piratas/wiki-piratas>
* <https://gitlab.com/piratas/wiki-piratas>
* <https://github.com/piratas/wiki-piratas>

---

# Toda a wiki

[[!map pages="wiki/* and !*/discussão" show=title]]

[0]: http://partidopirata.org
[1]: https://gti.partidopirata.org

[[!sidebar content="""

**Atalhos**

[[Como editar esta wiki|tutorial/editar a wiki]]  
[[Documentos do Partido|wiki/Documentos]]  
[[Infraestrutura de TI do Partido|wiki/infra]]  

**Ferramentas**

[[Tutoriais]]  
[[Reuniões]]  

**Projetos**

[[Coleta de assinaturas|projeto/coleta]]  
[[Reformulação da gestão de crises|wiki/gestão de crises]]  
"""]]

[[!meta title="Wiki Pirata"]]
